#pragma strict


var forwardRate : float = 3;
var turnRate : float = 2; 


function Update () {

// tank's forward speed in action 
//up and down arrow keys or "w" and "s" keys
var forwardMoveAmount = Input.GetAxis("Vertical") * forwardRate;

// force of tank's  turn
var turnForce = Input.GetAxis("Horizontal") * turnRate;

//rotate tank in action
transform.Rotate(0, turnForce, 0 );

// transform.position plus the formula will be the sum of transform.position  
transform.position += transform.forward * forwardMoveAmount * Time.deltaTime;


}