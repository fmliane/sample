#pragma strict

var prefabCannonBall:Rigidbody;
var shootForce:float;
var shootPosition:Transform;

function Update()
{
	if(Input.GetButtonDown("Jump"))
	{
	//add physics force to cannon ball 
	var instanceCannonBall = Instantiate(prefabCannonBall, transform.position, shootPosition.rotation);
	instanceCannonBall.rigidbody.AddForce(shootPosition.right * shootForce);

	}}